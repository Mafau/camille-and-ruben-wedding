import gsap, { Power2, Power1 } from 'gsap'
import Vue from 'vue'
import { Component, Prop } from 'vue-property-decorator'
import { CreateElement } from 'vue/types/umd'
import AnimationContent from './AnimationContent'

@Component({
  components: {
    AnimationContent
  }
})
export default class TitleAnimation extends Vue {
  @Prop() readonly content!: string

  animationLeaveCallback(element: Element) {
    return new Promise(resolve => {
      const tl = gsap.timeline({
        onComplete: () => resolve()
      })

      tl.to(
        element,
        {
          duration: 0.6,
          opacity: 0,
          ease: Power2.easeIn
        },
        'start'
      )

      tl.to(
        element,
        {
          duration: 1,
          y: -120,
          ease: Power1.easeIn
        },
        'start'
      )
    })
  }

  animationEnterCallback(element: Element) {
    return new Promise(resolve => {
      const tl = gsap.timeline({
        onComplete: () => resolve()
      })

      tl.set(element, {
        y: 20
      })

      tl.to(element, {
        opacity: 1,
        y: 0
      })
    })
  }

  onAnimation(e: boolean) {
    this.$emit('onAnimation', e)
  }

  render(h: CreateElement) {
    return h(AnimationContent, {
      props: {
        content: this.content,
        tag: 'h4',
        animationEnterCallback: this.animationEnterCallback,
        animationLeaveCallback: this.animationLeaveCallback
      },
      on: {
        onAnimation: this.onAnimation
      }
    })
  }
}
