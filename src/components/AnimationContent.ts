import Vue, { CreateElement } from 'vue'
import Component from 'vue-class-component'
import { Prop, Watch } from 'vue-property-decorator'
import imagesLoaded from 'imagesloaded'

@Component
export default class AnimationContent extends Vue {
  @Prop() readonly content!: string
  @Prop() readonly tag!: string
  @Prop() readonly animationLeaveCallback!: (element: Element) => Promise<void>
  @Prop() readonly animationEnterCallback!: (element: Element) => Promise<void>

  currentContent = ''

  render(h: CreateElement) {
    if (this.tag === 'img') {
      return h(this.tag, {
        ref: 'content',
        attrs: {
          src: this.currentContent
        }
      })
    }
    return h(
      this.tag,
      {
        ref: 'content'
      },
      this.currentContent
    )
  }

  created() {
    this.currentContent = this.content
  }

  @Watch('content')
  async onContentChange(val: string) {
    this.$emit('onAnimation', true)
    await this.animationLeaveCallback.call(this, this.$refs.content as Element)
    this.currentContent = val
    imagesLoaded(this.$refs.content as Element, async () => {
      await this.animationEnterCallback.call(
        this,
        this.$refs.content as Element
      )
      this.$emit('onAnimation', false)
    })
  }
}
