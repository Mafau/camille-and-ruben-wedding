import gsap, { Power1 } from 'gsap'
import Vue from 'vue'
import { Component, Prop } from 'vue-property-decorator'
import { CreateElement } from 'vue/types/umd'
import AnimationContent from './AnimationContent'

@Component({
  components: {
    AnimationContent
  }
})
export default class ImageAnimation extends Vue {
  @Prop() readonly content!: string

  animationLeaveCallback(element: Element) {
    return new Promise(resolve => {
      const tl = gsap.timeline({
        onComplete: () => resolve()
      })

      const { width, height } = element.getBoundingClientRect()

      tl.set(element, {
        duration: 1,
        clip: `rect(0px,${width}px,${height}px,0px)`,
        ease: Power1.easeIn
      })

      tl.to(element, {
        duration: 0.5,
        clip: `rect(${height}px,${width}px,${height}px,0px)`
      })
    })
  }

  animationEnterCallback(element: Element) {
    return new Promise(resolve => {
      const tl = gsap.timeline({
        onComplete: () => resolve()
      })
      const { width, height } = element.getBoundingClientRect()

      tl.set(element, {
        clip: `rect(0px,${width}px,0px,0px)`
      })

      tl.to(element, {
        delay: 0.5,
        duration: 1,
        clip: `rect(0px,${width}px,${height}px,0px)`,
        ease: Power1.easeOut
      })
    })
  }

  onAnimation(e: boolean) {
    this.$emit('onAnimation', e)
  }

  render(h: CreateElement) {
    return h(AnimationContent, {
      props: {
        content: this.content,
        tag: 'img',
        animationEnterCallback: this.animationEnterCallback,
        animationLeaveCallback: this.animationLeaveCallback
      },
      on: {
        onAnimation: this.onAnimation
      }
    })
  }
}
