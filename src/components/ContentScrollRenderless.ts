import { VNode } from 'vue'
import Vue from 'vue'
import Component from 'vue-class-component'
import { Prop } from 'vue-property-decorator'
import { CreateElement } from 'vue/types/umd'

export interface SliderItemInterface {
  title: string
  img: string
  category: string
}

@Component
export default class ContentScrollRenderless extends Vue {
  @Prop() readonly content!: SliderItemInterface[]
  @Prop() readonly onAnimation!: boolean

  currentIndex = 0

  mounted() {
    ;(this.$refs.viewContainer as HTMLElement).addEventListener<'wheel'>(
      'wheel',
      event => {
        if (!this.onAnimation) {
          if (event.deltaY < 0 && this.currentIndex > 0) {
            this.currentIndex--
          } else if (event.deltaY > 0 && this.currentIndex < this.maxItem - 1) {
            this.currentIndex++
          }
        }
      }
    )
  }

  get currentItem(): SliderItemInterface {
    return this.content[this.currentIndex]
  }

  get maxItem(): number {
    return this.content.length
  }

  render(h: CreateElement): any {
    return h(
      'div',
      {
        class: 'view-container',
        ref: 'viewContainer'
      },
      this.$scopedSlots.default!({
        currentItem: this.currentItem,
        currentIndex: this.currentIndex,
        maxItem: this.maxItem
      })
    )
  }
}
